# Semestrálna práca Selenium - [Heureka](https://www.heureka.cz/)

> **Předmět:** B6B36TS1
> 
> **Autoři:**
> - cernym68
> - figuldan


<!-- TOC -->
* [Semestrálna práca Selenium - Heureka](#semestrálna-práca-selenium---heureka)
  * [Popis práce](#popis-práce)
    * [Prehľad častí aplikácie](#prehľad-častí-aplikácie)
    * [Priority častí aplikácie](#priority-častí-aplikácie)
    * [Test levels](#test-levels)
  * [Testovacie scenáre](#testovacie-scenáre)
    * [Mezní podmínky EC](#mezní-podmínky-ec)
    * [Kombinácie testovacích dát pairwise testing](#kombinácie-testovacích-dát-pairwise-testing)
    * [Procesné diagramy](#procesné-diagramy)
  * [Detailné testovacie scenáre](#detailné-testovacie-scenáre)
    * [Testovací scénář č. 1 - Testování Mazání předmětů z košíku](#testovací-scénář-č-1---testování-mazání-předmětů-z-košíku)
      * [Prerekvizity: Manuální přidání produktů do košíku](#prerekvizity--manuální-přidání-produktů-do-košíku)
      * [Kroky:](#kroky-)
    * [Testovací scenár č. 2](#testovací-scenár-č-2)
  * [Tests](#tests)
  * [Parametrized tests](#parametrized-tests)
<!-- TOC -->

## Popis práce

Táto semestrálna práca testuje jednu z najpopulárnejších stránok pre online nakupovanie - [heureka.cz](https://www.heureka.cz/).
Stránku pre vypracovanie sme si vybrali z dôvodu širokej funkcionality a množstvu sortimentov, ktoré táto stránka ponúka pre nakupovanie.
Pre testovanie stránky sme použili [Selenium framework](https://www.selenium.dev/).

### Prehľad častí aplikácie 

Webová stránka ponúka rôzne funkcionality, ktoré užívateľ môže využívať pre online nakupovanie. Funkcionality, ktoré sme sa rozhodli otestovať sú následovné:

- **Prihlasovanie**
- **Presmerovanie pri nakupovaní produktov mimo stránku Heureka**
- **Pridávanie produktov do košíka**
- **Odoberanie produktov z košíka**
- **Vyhľadávanie produktov**
- **Pokročilé vyhľadávanie**
- **Obľúbené produkty**
- **Zoradenie produktov podľa cien**
- **Porovnávanie produktov**
- **Stráženie cien** 

Pre testovanie sme používali Page Object Models, pre ktorú každý model reprezentuje jednu stránku. 

### Priority častí aplikácie


|          Proces          |                   Podproces                   |             Požiadavka              | Možné poškodenie |     Vysvetlenie poškodenia     | Časť systému  | Prav. zlyhania |      Vysvetlenie prav. zlyhania       | Trieda rizika |
|:------------------------:|:---------------------------------------------:|:-----------------------------------:|:----------------:|:------------------------------:|:-------------:|:--------------:|:-------------------------------------:|:-------------:|
|       Nakupovanie        |   Pridávanie/odoberanie produktov z košíka    |        Nakupovanie produktov        |        H         | Zlyhanie hlavnej funkcionality |    Klient     |       L        |    Najviac používaná funkcionalita    |       A       |
|      Presmerovanie       |    Presmerovanie do iného obchodu produktu    |      Nakupovanie mimo heuréku       |        L         |   Nesprávny link k produktu    |    Server     |       L        | Zle generovaný link na strane serveru |       B       |
| Manipulácia s produktami | Porovnávanie, obľúbené produkty, hlídaní cien | Organizácia produktov pre užívateľa |        L         | Nesprávne mapovanie produktov  | Klient/Server |       L        |     Menej používané funkcionality     |       C       |
|       Vyhľadávanie       |                 Našepkávanie                  | Správne vyhľadávanie a našepkávanie |        M         |       Nesprávne výsledky       | Klient/server |       L        |                   -                   |       C       |

### Test levels

|        Části systému        | třída rizika | Vývojářské/unit testy | Systémové testy |  UAT   | Testy po spuštění v produkci |
|:---------------------------:|:------------:|:---------------------:|:---------------:|:------:|:----------------------------:|
|      **funkcionalita**      |              |                       |                 |        |                              |
|         Nakupování          |      A       |        vysoká         |                 | vysoká |             ano              |
|        Nákupní košík        |      A       |        vysoká         |                 | vysoká |             ano              |
|        Přihlašování         |      A       |        vysoká         |                 | nízká  |             ano              |
|         Vyhledávání         |      B       |         nízká         |                 | nízká  |             ano              |
|     Porovnání produktů      |      C       |         nízká         |                 | nízká  |              ne              |
|      Oblíbené produkty      |      C       |         nízká         |                 | nízká  |              ne              |
|       třídění obchodů       |      C       |         nízká         |                 | nízká  |              ne              |
| **Uživatelská přívětivost** |              |                       |                 |        |                              |
|         Nakupování          |      A       |                       |     vysoká      | vysoká |             ano              |
|        Nákupní košík        |      A       |                       |     vysoká      | vysoká |             ano              |
|        Přihlašování         |      A       |                       |      nízká      | nízká  |             ano              |
|         Vyhledávání         |      B       |                       |     vysoká      | nízká  |             ano              |
|     Porovnání produktů      |      C       |                       |     vysoká      | nízká  |              ne              |
|      Oblíbené produkty      |      C       |                       |      nízká      | nízká  |              ne              |
|       Třídění obchodů       |      C       |                       |      nízká      | nízká  |              ne              |

## Testovacie scenáre

### Mezní podmínky EC

Mezní podmínky se vyskytují v rozšířeném vyhledávání. Avšak mezi filtry se převážně vyskytují **nominální** (kategorické) proměnné.
Ty jsou klasicky voleny checkboxy. 

Následně se zřídka objevují intervalové proměnné, např. šířka záběru sekačky. 
Jejich třídy ekvivalence se liší v závislosti na zboží. 
Většinou ale z business hlediska nebude šířka záběru záporné číslo. 
Technická třída ekvivalence by ale byla samotná velikost proměnné.

### Kombinácie testovacích dát pairwise testing

Pre pokročilé vyhľadávanie sme vytvorili kombinácie testovacích dát technikou pairwise testing. Testovacie parametre sme určili následovné:

| Parameter |                                 Sada parametrov                                 |
|:---------:|:-------------------------------------------------------------------------------:|
| minPrice  |                              [1000, 4000, 10_000]                               |
| maxPrice  |                             [8000, 18_100, 22_500]                              |
|   brand   |                        [Kärcher, Parkside, Bosch, Dyson]                        |


Technika pairwise testing nám tak vytvorila následujúcu kombináciu parametrov Strength - 2:

| minPrice | maxPrice | brand    |
|:--------:|:--------:|----------|
|   1000   |  18000   | Kärcher  |
|   4000   |  22500   | Kärcher  |
|   1000   |  22500   | Parkside |
|   4000   |   8000   | Parkside |
|  10000   |  18000   | Parkside |
|   1000   |   8000   | Bosch    |
|   4000   |  18000   | Bosch    |
|  10000   |  22500   | Bosch    |
|   1000   |   8000   | Dyson    |
|   4000   |  18000   | Dyson    |
|  10000   |  22500   | Dyson    |

### Procesné diagramy

Procesné diagramy, ktoré sa zamieravajú na hlavné procesy tejto stránky:

1. Prihlasovanie a pridávanie produktov do košíka. Po pridaní sa prejde na platbu.

![process-diagram-1](src/main/resources/images/shopping-process-diagram.png)

Orientovaný graf testovacieho scenára:

![process-diagram-1](src/main/resources/images/shopping-process-graph.png)

Pre ktorý vznikajú tieto testovacieho scenáre s pokrytím TDL 2:

| Scenár |                                Sekvencia testov                                 |
|:------:|:-------------------------------------------------------------------------------:|
| No. 1  |                       1 - 2 - 3 - 14 - 15 - 16 - 17 - 18                        |
| No. 2  |     1 - 2 - 4 - 5 - 7 - 9 - 10 - 11 - 10 - 12 - 13 - 14 - 15 - 16 - 17 - 18     |
| No. 3  |         1 - 2 - 4 - 6 - 10 - 11 - 10 - 12 - 13 - 14 - 15 - 16 - 17 - 18         |
| No. 4  | 1 - 2 - 4 - 5 - 7 - 8 - 7 - 9 - 10 - 11 - 10 - 12 - 13 - 14 - 15 - 16 - 17 - 18 |

2. Uživatelská interakce - základní akce, které může uživatel provést.

![process-diagram-1](src/main/resources/images/uživatelská%20interakce1.png)

Orientovaný graf testovacího scénáře

![process-diagram-1](src/main/resources/images/orientedGraph.png)

| Scénář |       Sekvencia testov        |
|:------:|:-----------------------------:|
| No. 1  |   1 - 2 - 3 - 4 - 3 - 5 - 6   |
| No. 2  |     1 - 2 - 3 - 7 - 8 - 9     |
| No. 3  | 1 - 2 - 3 - 10 - 11 - 12 - 13 |


## Detailné testovacie scenáre

### Testovací scénář č. 1 - Testování Mazání předmětů z košíku
Tento test přidává do košíku zadané produkty a následně je maže. Postup je stejný jako u RemoveItemsFromShoppingCastTest.java
- Závažnost chyby: A
- Autor: Cernym68

#### Prerekvizity: Manuální přidání produktů do košíku
- Obrazovka >= 1920x1080
- Přidání 3 produktů ze souboru /resources/dataToRemoveItems.txt do košíku

#### Kroky:
- Otevřít stránku www.Heureka.cz
- Přejít do košíku kliknutím na ikonu košíku
- Opakovat toto pro každý prvek v seznamu:
  - Klikneme na křížek (vymazání produktu)
  - Ve vyskakovacím okně potvrdíme stisknutím červeného tlačítka
  - Počkáme na načtení stránky
- Zkontrolujeme, že v košíku není žádný produkt. Na stránce očekáváme hlášku: *Váš košík zeje prázdnotou...*
- Pakliže splněno, test byl úspěšný.

### Testovací scenár č. 2 
**Autor:** figuldan <br>
**Názov:** Pokročilé vyhľadávanie <br>
**Zhrnutie:** Porovnávanie produktov na základe parametrov vyhľadávania <br>
**Popis:** Užívateľ klikne na sekciu "Bíle zboží" a potom na "Vysávače". Pozerá si produkty v 
rámci vyhľadávaných parametrov ceny a značky produktu. <br>
**Vstupné podmienky:** Žiadne <br>
**Testovacie dáta:**
- Minimálna cena za produkt: nesmie presiahnuť maximálnu cenu
- Maximálna cena za produkt: nesmie klesnúť pod minimálnu cenu
- Značka produktu: Značka ktorú očakávame vyhľadať

**Očakávaný výsledok:** Produkt bude mať cenu v rozmedzí minimálnej a maximálnej ceny a bude sedieť aj
so značkou vyhľadaného produktu. 

## Tests

>- [x] Login - _**figuldan**_
>- [x] Check shop item redirecting - **_figuldan_**
>- [x] Adding items to shopping cart - **_figuldan_**
>- [x] Removing items from shopping cart - **_cernym68_**
>- [x] Search - **_cernym68_**
>- [x] Advanced search - **_figuldan_**
>- [x] Liked items - **_cernym68_**
>- [x] Sorting prices - **_cernym68_**
>- [X] Product comparison - **_figuldan_**
>- [x] Price checking - **_cernym68_**

## Parametrized tests

>- [x] Search - **_cernym68_**
>- [x] Advanced search - **_figuldan_**