package models;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdvancedSearchPage {
    private final ChromeDriver driver;

    public AdvancedSearchPage(ChromeDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void setUp(String url) {
        driver.get(url);
    }

    public String getCurrentUrl() {
        return driver.getCurrentUrl();
    }

    @FindBy(xpath = "//*[@id=\"price-range-min\"]")
    private WebElement minimumPriceInput;

    @FindBy(xpath = "//*[@id=\"price-range-max\"]")
    private WebElement maximumPriceInput;

    @FindBy(xpath = "//*[@id=\"3326\"]")
    private WebElement brandSearch;

    @FindBy(xpath = "/html/body/div[4]/div/div/div/div/aside/div[1]/div/ul/li[5]/div/div/div/div/div/ul/li/div/label[1]")
    private WebElement brandCheckBox;

    @FindBy(xpath = "/html/body/div[4]/div/div/div/div/main/section[1]/div[1]/ul/li[1]/section/div/div/div[1]/h3/a")
    private WebElement productName;

    @FindBy(xpath = "/html/body/div[4]/div/div/div/div/main/section[1]/div[1]/ul/li[1]/section/div/div/div[2]/div/a[1]/span")
    private WebElement productPrice;

    public void insertMinimumPrice(int minimumPrice) {
        minimumPriceInput.sendKeys(Integer.toString(minimumPrice));
    }

    public void insertMaximumPrice(int maximumPrice) {
        maximumPriceInput.sendKeys(Integer.toString(maximumPrice));
    }

    public void insertVacuumBrand(String brand) {
        brandSearch.sendKeys(brand);
    }

    public void checkBrandType() {
        brandCheckBox.click();
    }

    public int[] parsePriceRange() {
        String productPriceRange = productPrice.getText();

        String[] priceRange = productPriceRange.split(" – ");
        String minPrice = priceRange[0].replaceAll("[^0-9.]", "");
        String maxPrice = priceRange[1].replaceAll("[^0-9.]", "");
        maxPrice = maxPrice.replace("Kč", "");

        int minPriceParsed = Integer.parseInt(minPrice);
        int maxPriceParsed = Integer.parseInt(maxPrice);

        return new int[] {minPriceParsed, maxPriceParsed};
    }

    public String getProductName(int item) {
        String productNameXpath = "/html/body/div[4]/div/div/div/div/main/section[1]/div[1]/ul/li[" + item + "]/section/div/div/div[1]/h3/a";
        WebElement productName = driver.findElement(By.xpath(productNameXpath));
        return productName.getText();
    }

    public void clickItemToComparison(int item) {
        String elementXpath = "/html/body/div[4]/div/div/div/div/main/section[1]/div[1]/ul/li[" + item + "]/section/div/ul/li/button/span";
        WebElement compareButton = driver.findElement(By.xpath(elementXpath));
        compareButton.click();
    }

    public void goToComparisonPage() {
        WebElement productComparisonPageButton = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/header/div/ul/li[1]/a"));
        productComparisonPageButton.click();
    }

    public String getProductName() {
        return productName.getText();
    }
}
