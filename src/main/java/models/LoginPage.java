package models;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

    private ChromeDriver driver;

    public LoginPage(ChromeDriver driver) {
        this.driver = driver;
    }

    public void setUp(String url) {
        System.out.println("setup the login page"); //testing
        driver.get(url);
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "frm-loginForm-loginForm-email")
    private WebElement userEmailInput;

    @FindBy(id = "frm-loginForm-loginForm-password")
    private WebElement userPasswordInput;

    @FindBy(xpath = "/html/body/div[4]/div/div/div/main/form/fieldset/footer/button")
    private WebElement loginButton;

    public void login() {
        userEmailInput.sendKeys("figuldan@fel.cvut.cz");
        userPasswordInput.sendKeys("79H6tpf*]]H&-9y");
        loginButton.click();
    }

    public String getCurrentPage() {
        return driver.getCurrentUrl();
    }

}
