package models;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class MainPage {

    private final ChromeDriver driver;

    public MainPage(ChromeDriver driver) {
        this.driver = driver;
    }

    public void setUp() {
        driver.get("https://www.heureka.cz/");
        PageFactory.initElements(driver, this);
    }

    public void setUp(String url) {
        driver.get(url);
    }

    @FindBy(xpath = "/html/body/div[2]/div/div/header/div/ul/li[4]/a")
    private WebElement loginButton;

    @FindBy(xpath = "//*[@id=\"didomi-notice-agree-button\"]")
    private WebElement acceptCookiesButton;

    @FindBy(xpath = "//*[@id=\"head-root\"]/header/div/ul/li[4]/a/span[2]")
    private WebElement userName;

    @FindBy(xpath = "/html/body/div[2]/div/div/header/div/form/input")
    private WebElement searchBar;

    @FindBy(xpath = "/html/body/div[2]/div/div/header/div/form/button[2]")
    private WebElement searchEnter;

    @FindBy(xpath = "/html/body/div[2]/div/div/header/div/ul/li[4]/a")
    private WebElement account;

    @FindBy(xpath = "/html/body/div[2]/div/div/header/div/ul/li[4]/ul/li[5]/a")
    private WebElement priceCheckButtonProfile;

    @FindBy(xpath = "/html/body/div[2]/div/div/header/div/ul/li[3]/a")
    private WebElement cart;

    public void clickPriceChecks(){
        account.click();
        priceCheckButtonProfile.click();
    }

    public void acceptCookies() {
        acceptCookiesButton.click();
    }

    public void goToLoginPage() {
        loginButton.click();
    }

    public String getCurrentPage() {
        return driver.getCurrentUrl();
    }

    public String getLoggedUser() {
        return userName.getText();
    }

    public void clickElectronicsSection() {
        WebElement electronicsSection = driver.findElement(By.xpath("//*[@id=\"heading-link-659\"]"));
        electronicsSection.click();

//        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void clickCart(){
        cart.click();
    }

    public void clickElectronicProductSection(int count) {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String sectionXpath = "/html/body/div[3]/div/div[1]/nav/ul/li[2]/div/div[2]/div[" + count + "]/div[2]/a[1]/span";

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); // this in combination with explicit wait should fix the problem
            WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
            WebElement orderButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(sectionXpath)));
            orderButton.click();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
//            WebElement section = driver.findElement(By.xpath(sectionXpath));
//            section.click();
    }

    public void setSearchBar(String text) {
        this.searchBar.sendKeys(text);
        this.searchEnter.click();
    }


    public void clickPhoneSection() {
        WebElement electronicsPage = driver.findElement(By.xpath("//*[@id=\"navigation-app\"]/div[1]/nav/ul/li[2]/div/div[2]/div[1]/div[2]/a[1]"));
        electronicsPage.click();
    }

    public void clickVacuumSection() {
        WebElement whiteGoodsSection = driver.findElement(By.xpath("//*[@id=\"heading-link-939\"]"));
        whiteGoodsSection.click();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        WebElement vacuumsSection = driver.findElement(By.xpath("//*[@id=\"navigation-app\"]/div[1]/nav/ul/li[4]/div/div[2]/div[1]/div[2]/ul/li[1]/a"));
        vacuumsSection.click();
    }
}
