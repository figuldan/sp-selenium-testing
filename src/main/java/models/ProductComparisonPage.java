package models;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProductComparisonPage {

    private ChromeDriver driver;

    public ProductComparisonPage(ChromeDriver driver) {
        this.driver = driver;
    }

    public void setUp(String url) {
        driver.get(url);
    }

    public String getProductName(int item) {
        String productNameXpath = "/html/body/div[3]/div/div/main/div[3]/div/table/tbody/tr[1]/td[" + item + "]/section/div/div[1]/h3/a";
        WebElement productName = driver.findElement(By.xpath(productNameXpath));
        return productName.getText();
    }
}
