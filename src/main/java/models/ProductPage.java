package models;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class ProductPage {

    private ChromeDriver driver;

    public ProductPage(ChromeDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void setUp(String url) {
        driver.get(url);
    }

    @FindBy(xpath = "//*[@id=\"__next\"]/div/main/div[1]/div[2]/h1")
    private WebElement itemName;

    @FindBy(css = "#__next > div > main > div.l-product-detail.o-wrapper.o-wrapper--fill-large-margin > div.l-product-detail__wrapper > div > div.l-product-detail__content > section > div.c-offers-container.c-offers-container--background.overflowing--fill-medium-margin > div.c-offers-sort-filter > div > div.c-offers-sort-filter__sort.u-hide\\@lteLine > div > ul > li:nth-child(2) > a")
    private WebElement nejlevnejsiButton;

    @FindBy(xpath = "/html/body/div[4]/div/div/main/div[1]/div[3]/a[1]")
    private WebElement likeButton;

    @FindBy(xpath = "/html/body/div[4]/div/div/main/div[2]/div[1]/div/div[2]/section/div[3]/section/div[2]/button")
    private WebElement setPriceChecking;

    public void goToShop(int buttonOrder) {
        String shopButtonXpath = "//*[@id=\"__next\"]/div/main/div[2]/div[1]/div/div[2]/section/div[1]/div[2]/div/div[2]/section[" + buttonOrder + "]/div/div[4]/a";
        WebElement shopButton = driver.findElement(By.xpath(shopButtonXpath));
        shopButton.click();
    }

    public void nejlevnejsiButtonClick() {
        nejlevnejsiButton.click();
    }

    public String getShopUrl(int shopOrder) {
        String shopUrlXpath = "//*[@id=\"__next\"]/div/main/div[2]/div[1]/div/div[2]/section/div[1]/div[2]/div/div[2]/section[" + shopOrder +"]/div/div[1]/a";
        return driver.findElement(By.xpath(shopUrlXpath)).getAttribute("href");
    }

    public void clickPriceCheckingButton() {
        setPriceChecking.click();
    }

    public void clicklikeButton() {
        likeButton.click();
    }

    public void clickBuyButton() {
//        WebElement orderButton = driver.findElement(By.xpath("//*[@id=\"__next\"]/div/main/div[1]/div[2]/div[4]/div/div[2]/button"));

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        WebElement orderButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"__next\"]/div/main/div[1]/div[2]/div[4]/div/div[2]/button")));
        orderButton.click();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public String getCurrentUrl() {
        return driver.getCurrentUrl();
    }
    //used to navigate back.
    public void goBack() {
        driver.navigate().back();
    }
}
