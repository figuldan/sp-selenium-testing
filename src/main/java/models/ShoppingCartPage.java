package models;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ShoppingCartPage {

    private ChromeDriver driver;

    public ShoppingCartPage(ChromeDriver driver) {
        this.driver = driver;
    }

    public void setUp(String url) {
        driver.get(url);
    }

    public String getProductUrl(int item) {
        String productXpath = "/html/body/div[3]/div/div[1]/div[" + item + "]/div/div/div[2]/div[1]/span[1]/a";
        WebElement productUrl = driver.findElement(By.xpath(productXpath));
        return productUrl.getAttribute("href");
    }
}
