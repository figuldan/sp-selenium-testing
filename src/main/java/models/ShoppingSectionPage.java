package models;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ShoppingSectionPage {

    private ChromeDriver driver;
    public ShoppingSectionPage(ChromeDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "/html/body/div[2]/div/div[2]/header/div/ul/li[3]/a")
    private WebElement shoppingCart;

    @FindBy(xpath = "/html/body/div[3]/div/div[1]/div[3]/div/div/div[1]/a")
    private WebElement firstDeleteButton;

    @FindBy(xpath = "/html/body/div[3]/div/div[1]/div[4]/div/div/div[1]/a")
    private WebElement secondDeleteButton;



    public void setUp(String url) {
        driver.get(url);
    }

    public void goToShoppingItem(int itemCount) {
        String itemXpath = "//*[@id=\"root\"]/div/div/div/main/section/div/ul/li[" + itemCount + "]/section/a[1]/img";
        WebElement shoppingItem =  driver.findElement(By.xpath(itemXpath));
        shoppingItem.click();
    }

    public String getCurrentUrl() {
        return driver.getCurrentUrl();
    }

    public void goToShoppingCart() {
        shoppingCart.click();
    }

}
