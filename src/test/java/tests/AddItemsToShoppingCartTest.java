package tests;

import models.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.chrome.ChromeDriver;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AddItemsToShoppingCartTest {
    private MainPage mainPage;
    private ChromeDriver driver;
    private LoginPage loginPage;
    private ProductPage productPage;
    private ShoppingSectionPage shoppingPage;
    private ShoppingCartPage shoppingCartPage;

    @BeforeEach
    public void setUp() {
        this.driver = new ChromeDriver();
        this.mainPage = new MainPage(driver);
        this.loginPage = new LoginPage(driver);
        this.productPage = new ProductPage(driver);
        this.shoppingPage = new ShoppingSectionPage(driver);
        this.shoppingCartPage = new ShoppingCartPage(driver);

        Dimension newDimension = new Dimension(1920, 1080);
        driver.manage().window().setSize(newDimension);
    }

    private void login() {
        mainPage.setUp();
        mainPage.acceptCookies();
        mainPage.goToLoginPage();

        loginPage.setUp(mainPage.getCurrentPage());
        loginPage.login();

        mainPage.setUp();
        String loggedUser = mainPage.getLoggedUser();
        assertEquals("figuldan@fel.cvut.cz", loggedUser);
    }

    @Test
    public void addItemsToShoppingCartTest() throws URISyntaxException {
        List<String> productURLs = new ArrayList<>();

        login();

        mainPage.clickElectronicsSection();
        mainPage.clickElectronicProductSection(2);

        shoppingPage.setUp(mainPage.getCurrentPage());

        for (int product = 1; product < 4; product++) {
            shoppingPage.goToShoppingItem(product);

            String pageUrl = shoppingPage.getCurrentUrl();
            productURLs.add(pageUrl);
            productPage.setUp(pageUrl);

            productPage.clickBuyButton();
            driver.navigate().back();
        }

        shoppingPage.goToShoppingCart();

        int product = 3;
        for (String productUrl : productURLs)
        {
            shoppingCartPage.setUp(shoppingPage.getCurrentUrl());
            String shoppingCartProduct = shoppingCartPage.getProductUrl(product);

            URI uri1 = new URI(productUrl);
            URI uri2 = new URI(shoppingCartProduct);

            boolean isSameBase = uri1.getHost().equals(uri2.getHost());
            boolean isSamePath = uri1.getPath().equals(uri2.getPath());

            assertTrue(isSameBase);
            assertTrue(isSamePath);

            product++;
        }
    }


}
