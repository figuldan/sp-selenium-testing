package tests;

import models.AdvancedSearchPage;
import models.MainPage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class AdvancedSearchTest {
    private MainPage mainPage;
    private AdvancedSearchPage advancedSearchPage;

    @BeforeEach
    public void setUp() {
        ChromeDriver driver = new ChromeDriver();
        this.mainPage = new MainPage(driver);
        this.advancedSearchPage = new AdvancedSearchPage(driver);
        Dimension newDimension = new Dimension(1920, 1080);
        driver.manage().window().setSize(newDimension);
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/advancedSearchParData.csv", encoding = "windows-1250", delimiter = ':')
    public void advancedSearchTest(int minimumPrice, int maximumPrice, String brand) {
        mainPage.setUp();
        mainPage.acceptCookies();
        System.out.println(minimumPrice);
        System.out.println(maximumPrice);
        System.out.println(brand);
        mainPage.clickVacuumSection();
        advancedSearchPage.setUp(mainPage.getCurrentPage());
        advancedSearchPage.insertMinimumPrice(minimumPrice);
        advancedSearchPage.insertMaximumPrice(maximumPrice);
        advancedSearchPage.insertVacuumBrand(brand);
        advancedSearchPage.checkBrandType();

        String productName = advancedSearchPage.getProductName();
        int[] parsedPrice = advancedSearchPage.parsePriceRange();

        boolean isNotLower = parsedPrice[0] >= minimumPrice;
        boolean isNotHigher = parsedPrice[1] <= maximumPrice;
        boolean isBrandType = productName.contains(brand);

        assertTrue(isNotLower);
        assertTrue(isNotHigher);
        assertTrue(isBrandType);
    }
}
