package tests;

import models.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.chrome.ChromeDriver;

public class LikedItemsTest {
    private MainPage mainPage;
    private ChromeDriver driver;
    private LoginPage loginPage;
    private ProductPage productPage;
    private ShoppingSectionPage shoppingPage;
    private ShoppingCartPage shoppingCartPage;

    @BeforeEach
    public void setUp() {
        this.driver = new ChromeDriver();
        this.mainPage = new MainPage(driver);
        this.loginPage = new LoginPage(driver);
        this.productPage = new ProductPage(driver);
        this.shoppingPage = new ShoppingSectionPage(driver);
        this.shoppingCartPage = new ShoppingCartPage(driver);

        Dimension newDimension = new Dimension(1920, 1080);
        driver.manage().window().setSize(newDimension);
    }

    private String login() {
        mainPage.setUp();
        mainPage.acceptCookies();
        mainPage.goToLoginPage();

        loginPage.setUp(mainPage.getCurrentPage());
        loginPage.login();

        mainPage.setUp();
        return mainPage.getLoggedUser();

    }

    @Test
    public void likedItemsTest(){
        //asserts if a login is ok
        Assertions.assertEquals(login(), "figuldan@fel.cvut.cz");
        //expected string
        String expected = "Google Pixel 7a 5G 8GB/128GB";
        //sends string to the searchbar and searches
        mainPage.setSearchBar(expected);
        // finds first search, returns its name and clicks it
        driver.findElement(By.xpath("/html/body/div[4]/div/div/div/div/main/section/div/ul/li[1]/section/div/div/div[1]/h3/a"))
                .click();
        // likes product
        productPage.clicklikeButton();
        try{
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // goes back
        productPage.goBack();
        // click liked products section
        driver.findElement(By.cssSelector("#head-root > header > div > ul > li.c-user-controls__item.c-user-controls__item--favorite > a"))
                .click();
        // asserts if a product is there
        Assertions.assertEquals(expected,driver.findElement(By.xpath("/html/body/div[4]/div/main/div/section[1]/div/section/header/h3/a"))
                .getText());
    }
}
