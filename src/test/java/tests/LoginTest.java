package tests;

import models.LoginPage;
import models.MainPage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LoginTest {

    private ChromeDriver driver;
    private MainPage mainPage;
    private LoginPage loginPage;

    @BeforeEach
    public void setUp() {
        this.driver = new ChromeDriver();
        this.loginPage = new LoginPage(driver);
        this.mainPage = new MainPage(driver);
        Dimension newDimension = new Dimension(1920, 1080);
        driver.manage().window().setSize(newDimension);
    }

    @Test
    public void loginTest() {
        mainPage.setUp();
        mainPage.acceptCookies();
        mainPage.goToLoginPage();

        loginPage.setUp(mainPage.getCurrentPage());
        loginPage.login();

        mainPage.setUp();
        String loggedUser = mainPage.getLoggedUser();
        assertEquals(loggedUser, "figuldan@fel.cvut.cz");
    }
}
