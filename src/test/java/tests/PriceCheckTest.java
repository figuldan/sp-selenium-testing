package tests;

import models.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class PriceCheckTest {
    private MainPage mainPage;
    private ChromeDriver driver;
    private LoginPage loginPage;
    private ProductPage productPage;
    private ShoppingSectionPage shoppingPage;
    private ShoppingCartPage shoppingCartPage;

    @BeforeEach
    public void setUp() {
        this.driver = new ChromeDriver();
        this.mainPage = new MainPage(driver);
        this.loginPage = new LoginPage(driver);
        this.productPage = new ProductPage(driver);
        this.shoppingPage = new ShoppingSectionPage(driver);
        this.shoppingCartPage = new ShoppingCartPage(driver);

        Dimension newDimension = new Dimension(1920, 1080);
        driver.manage().window().setSize(newDimension);
    }

    private String login() {
        mainPage.setUp();
        mainPage.acceptCookies();
        mainPage.goToLoginPage();

        loginPage.setUp(mainPage.getCurrentPage());
        loginPage.login();

        mainPage.setUp();
        return mainPage.getLoggedUser();

    }

    @Test
    public void priceCheckTest(){
        //asserts if a login is ok
        Assertions.assertEquals(login(), "figuldan@fel.cvut.cz");
        //expected string
        String expected = "Google Pixel 7a 5G 8GB/128GB";
        // initializing the scroller
        WebElement footer = driver.findElement(By.tagName("footer"));
        int deltaY = footer.getRect().y;
        //sends string to the searchbar and searches
        mainPage.setSearchBar(expected);
        // finds first search, returns its name and clicks it
        driver.findElement(By.xpath("/html/body/div[4]/div/div/div/div/main/section/div/ul/li[1]/section/div/div/div[1]/h3/a"))
                .click();


        new Actions(driver)
                .scrollByAmount(0, deltaY)
                .perform();
        try{
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        productPage.clickPriceCheckingButton();

        try{
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // sending keys to check the price
        driver.findElement(By.xpath("/html/body/div[4]/div/div/main/div[2]/div[1]/div/div[9]/aside/div/form/div/div/input"))
                .sendKeys("5000");
        // click set the price check
        driver.findElement(By.xpath("/html/body/div[4]/div/div/main/div[2]/div[1]/div/div[9]/aside/div/form/button"))
                .click();
        try{
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // goes back
        productPage.goBack();
        mainPage.setUp();
        mainPage.clickPriceChecks();
        Assertions.assertEquals(expected,driver.findElement(By.xpath("/html/body/div[4]/div/main/div/div/div/section/div/section/header/h3/a"))
                .getText());

        Assertions.assertEquals("5000",driver.findElement(By.xpath("/html/body/div[4]/div/main/div/div/div/section/div/section/div[2]/strong"))
                .getText()
                .substring(13,19)
                .replaceAll(" ", ""));
    }
}
