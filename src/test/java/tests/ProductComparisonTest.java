package tests;

import com.sun.tools.javac.Main;
import models.AdvancedSearchPage;
import models.LoginPage;
import models.MainPage;
import models.ProductComparisonPage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ProductComparisonTest {
    private ChromeDriver driver;
    private MainPage mainPage;
    private AdvancedSearchPage advancedSearchPage;
    private ProductComparisonPage productComparisonPage;
    private LoginPage loginPage;
    @BeforeEach
    public void setUp() {
        this.driver = new ChromeDriver();
        this.loginPage = new LoginPage(driver);
        this.mainPage = new MainPage(driver);
        this.advancedSearchPage = new AdvancedSearchPage(driver);
        this.productComparisonPage = new ProductComparisonPage(driver);

        Dimension newDimension = new Dimension(1920, 1080);
        driver.manage().window().setSize(newDimension);
    }
    private void login() {
        mainPage.setUp();
        mainPage.acceptCookies();
        mainPage.goToLoginPage();

        loginPage.setUp(mainPage.getCurrentPage());
        loginPage.login();

        mainPage.setUp();
        String loggedUser = mainPage.getLoggedUser();
        assertEquals("figuldan@fel.cvut.cz", loggedUser);
    }


    @Test
    public void productComparisonTest() {
        List<String> productNames = new ArrayList<>();

        login();

        mainPage.setUp();
        mainPage.clickVacuumSection();

        advancedSearchPage.setUp(mainPage.getCurrentPage());
        for (int item = 1; item <= 3; item++)
        {
            String itemName = advancedSearchPage.getProductName(item);
            productNames.add(itemName);
            advancedSearchPage.clickItemToComparison(item);
        }
        advancedSearchPage.goToComparisonPage();
        String productComparisonPageUrl = advancedSearchPage.getCurrentUrl();
        productComparisonPage.setUp(productComparisonPageUrl);

        for (int product = 2; product < productNames.size() + 2; product++)
        {
            String productName = productComparisonPage.getProductName(product);
            assertTrue(productNames.contains(productName));
        }
    }
}
