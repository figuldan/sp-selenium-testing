package tests;

import models.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * FIRSTLY RUN THE AddItemsToShoppingCartTest.java, otherwise this test will result in an error.
 */
public class RemoveItemsFromShoppingCartTest {
    private MainPage mainPage;
    private ChromeDriver driver;
    private LoginPage loginPage;
    private ProductPage productPage;
    private ShoppingSectionPage shoppingPage;
    private ShoppingCartPage shoppingCartPage;

    @BeforeEach
    public void setUp() {
        this.driver = new ChromeDriver();
        this.mainPage = new MainPage(driver);
        this.loginPage = new LoginPage(driver);
        this.productPage = new ProductPage(driver);
        this.shoppingPage = new ShoppingSectionPage(driver);
        this.shoppingCartPage = new ShoppingCartPage(driver);

        Dimension newDimension = new Dimension(1600, 900);
        driver.manage().window().setSize(newDimension);
    }

    private void login() {
        mainPage.setUp();
        mainPage.acceptCookies();
        mainPage.goToLoginPage();

        loginPage.setUp(mainPage.getCurrentPage());
        loginPage.login();

        mainPage.setUp();
        String loggedUser = mainPage.getLoggedUser();
        assertEquals(loggedUser, "figuldan@fel.cvut.cz");
    }

    @Test
    public void removeFromCart() {
        String expectedResult = "Váš košík zeje prázdnotou...";

        login();

        mainPage.clickCart();


//        mainPage.clickElectronicsSection();
//        mainPage.clickElectronicProductSection(5);
//        shoppingPage.setUp(mainPage.getCurrentPage());


//        for (int product = 1; product < 4; product++) {
//            shoppingPage.goToShoppingItem(product);
//
//            String pageUrl = shoppingPage.getCurrentUrl();
//            productURLs.add(pageUrl);
//            productPage.setUp(pageUrl);
//
//            productPage.clickBuyButton();
//            driver.navigate().back();
//        }
        shoppingCartPage.setUp(mainPage.getCurrentPage());
        for (int item = 0; item < 3; item++) {

        driver.findElement(By.xpath("/html/body/div[3]/div/div[1]/div[3]/div/div/div[1]/a"))
                .click();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); // this should fix any problems
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        WebElement removeButton = wait.until(ExpectedConditions.elementToBeClickable(By.className("e-button--negative")));

        removeButton.click();
    }
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();}

        String actualString = driver.findElement(By.cssSelector("body > div.scope-essentials-v7\\.4\\.1.scope-responsive-cart > div > div.c-empty-cart > h1"))
                .getText();

        Assertions.assertEquals(expectedResult,actualString);
    }
}
