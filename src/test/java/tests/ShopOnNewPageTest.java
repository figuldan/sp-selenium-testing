package tests;

import models.MainPage;
import models.ProductPage;
import models.ShoppingSectionPage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import java.awt.*;
import java.awt.event.KeyEvent;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.openqa.selenium.support.ui.ExpectedConditions.numberOfWindowsToBe;

public class ShopOnNewPageTest {

    private ChromeDriver driver;
    private MainPage mainPage;
    private ShoppingSectionPage shoppingSectionPage;
    private ProductPage productPage;
    private Actions actions;

    @BeforeEach
    public void setUp() {
        this.driver = new ChromeDriver();
        this.actions = new Actions(driver);
        this.mainPage = new MainPage(driver);
        this.shoppingSectionPage = new ShoppingSectionPage(driver);
        this.productPage = new ProductPage(driver);
        Dimension newDimension = new Dimension(1920, 1080);
        driver.manage().window().setSize(newDimension);
    }

    @Test
    public void shopOnNewPageTest() {
        mainPage.setUp();
        mainPage.acceptCookies();
        mainPage.clickElectronicsSection();
        mainPage.clickPhoneSection();

        shoppingSectionPage.setUp(mainPage.getCurrentPage());
        shoppingSectionPage.goToShoppingItem(1);

        productPage.setUp(shoppingSectionPage.getCurrentUrl());
        productPage.goToShop(1);

        String originalWindow = driver.getWindowHandle();

        for (String windowHandle : driver.getWindowHandles()) {
            if(!originalWindow.contentEquals(windowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }

        String currentUrl = driver.getCurrentUrl();
        assertEquals("https://www.ispace.cz/iphone-13-128gb-starlight.html", currentUrl);
    }


}
