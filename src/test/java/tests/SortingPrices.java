package tests;

import models.LoginPage;
import models.MainPage;
import models.ProductPage;
import models.ShoppingSectionPage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SortingPrices {
    private ChromeDriver driver;
    private MainPage mainPage;
    private LoginPage loginPage;
    private ShoppingSectionPage shoppingSectionPage;
    private ProductPage productPage;

    @BeforeEach
    public void setUp() {
        this.driver = new ChromeDriver();
        this.loginPage = new LoginPage(driver);
        this.mainPage = new MainPage(driver);
        this.shoppingSectionPage = new ShoppingSectionPage(driver);
        this.productPage = new ProductPage(driver);
        Dimension newDimension = new Dimension(1920, 1080);
        driver.manage().window().setSize(newDimension);
    }

    private void login(){
        mainPage.setUp();
        mainPage.acceptCookies();
        mainPage.goToLoginPage();

        loginPage.setUp(mainPage.getCurrentPage());
        loginPage.login();

        mainPage.setUp();
        String loggedUser = mainPage.getLoggedUser();
        assertEquals(loggedUser, "figuldan@fel.cvut.cz");
    }

    @Test
    public void pricesTest() {
        //logging the user
        login();

        //opening electronics and specific item
        mainPage.clickElectronicsSection();
        mainPage.clickPhoneSection();
        shoppingSectionPage.goToShoppingItem(1);

        productPage.nejlevnejsiButtonClick();

        // initializing the scroller
        WebElement footer = driver.findElement(By.tagName("footer"));
        int deltaY = footer.getRect().y;

        //assert for a specific number of times
        for (int itemIndex = 1; itemIndex < 8; itemIndex++) {

            // get the first item
            String firstItem = driver.findElement(
                    By.xpath("/html/body/div[4]/div/div/main/div[2]/div[1]/div/div[2]/section/div[1]/div[3]/div/div/section[" + itemIndex +"]/div/div[3]/a/span"))
                    .getText();
            firstItem = firstItem.substring(0, firstItem.length()-3);

            // Replace space with an empty string
            firstItem = firstItem.replace(" ", "");
            System.out.println(firstItem); // todo testing


            // get the second item
            int higherItemIndex = itemIndex+1;
            String secondItem = driver.findElement(
                    By.xpath("/html/body/div[4]/div/div/main/div[2]/div[1]/div/div[2]/section/div[1]/div[3]/div/div/section[" + higherItemIndex +"]/div/div[3]/a/span"))
                    .getText();
            secondItem = secondItem.substring(0, secondItem.length()-3);

            // Replace space with an empty string
            secondItem = secondItem.replace(" ", "");
            System.out.println(secondItem); // todo testing

            // Scrolling
            if(itemIndex%3 == 1) new Actions(driver)
                                    .scrollByAmount(0, deltaY)
                                    .perform();

            // Asserting if firstItem is smaller or equal to secondItem
            Assertions.assertTrue(Integer.parseInt(firstItem)<=Integer.parseInt(secondItem));
            System.out.println("Asserting whether is "+firstItem+" smaller than "+secondItem+" or not.");
        }

    }
}
